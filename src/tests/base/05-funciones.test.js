import { getUser, getUsuarioActivo } from "../../base/05-funciones";

describe('Pruebas en 05-funciones.js', () => {
    test('prueba en getUser', () => {
        const userTest = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }
        const user = getUser();

        expect(user).toEqual(userTest)
    });

    test('prueba en getUser(nobre)', () => {
    const nombre = 'Maria'
        const userTest1 = {
            uid: 'ABC567',
            username: nombre
        }
        const user1 = getUsuarioActivo(nombre);

        expect(user1).toEqual(userTest1)
    });

})
