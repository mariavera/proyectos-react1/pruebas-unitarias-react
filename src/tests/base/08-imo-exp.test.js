import { getHeroeById, getHeroesByOwner } from "../../base/08-imp-exp";
import heroes from "../../data/heroes";
describe('Pruebas de funciones heroes', () => {
    test('get heroes by id', () => {

        const id = 1;

        const heroe=getHeroeById(id);
        const heroData = heroes.find( (h)=> h.id===id);
        expect(heroe).toEqual(heroData)
    });

    test('undefined si heroe o existe', () => {

        const id = 10;

        const heroe=getHeroeById(id);
        expect(heroe).toBe(undefined)
    });

    
    test('get heroes by owner to equal ', () => {

        const ownerId = 'DC';

        const owner=getHeroesByOwner(ownerId);
        const ownerData=heroes.filter( (e)=> (e.owner===ownerId)  )
        expect(owner).toEqual(ownerData)
    });

    test('get heroes by owner to be ', () => {

        const ownerId = 'Marvel';

        const owner=getHeroesByOwner(ownerId);
        console.log(owner)
        const ownerData=heroes.filter( (e)=> e.owner===ownerId)
        console.log(ownerData)

        owner.forEach( (e,i)=>{

            expect(e).toBe(ownerData[i])
        })
    });
    
})
