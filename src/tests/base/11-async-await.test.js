import { getImagen } from "../../base/11-async-await"

describe('Pruebas con async - await', () => {
    test('Url de imagen ', async() => {
        const url =await getImagen();

        expect( url.includes('https://')).toBe(true)
    })
    
})
