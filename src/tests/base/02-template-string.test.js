import { getSaludo } from "../../base/02-template-string";

describe('Pruebas en 02-template-string.js', () => {
    test('prueba en getSaludo', () => {
        const nombre = 'Fernando';

        const saludo = getSaludo(nombre);

        expect(saludo).toBe('Hola ' + nombre)
    });

    test('prueba en getSaludo', () => {

        const saludo = getSaludo();

        expect(saludo).toBe('Hola carlos')
    })

})
