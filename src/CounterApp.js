
import React, { useState } from 'react';
import PropTypes from "prop-types";


const CounterApp = ({ value=0 }) => {
    // const saludo = {
    //     HOLA: 'SDSD'
    // };
    const [counter, setCounter] = useState(value);
    const handleAdd = (e) => {
        setCounter(counter + 1);
        // setCounter((c) => c + 1)
    };

    const handleReset = (e) => {
        setCounter(value);
        // setCounter((c) => c + 1)
    };

    const handleSubstract = (e) => {
        setCounter(counter - 1);
        // setCounter((c) => c + 1)
    };
    return (
        // <><pre>{JSON.stringify(saludo, null, 3)}</pre>
        <><h1>CounterApp</h1>
            <h2>{counter}</h2>
            <button onClick= {handleAdd} >+1</button>
            <button onClick= {handleReset} >Reset</button>
            <button onClick= {handleSubstract} >-1</button>
        </>

    );

}
CounterApp.propTypes = {
    value: PropTypes.number.isRequired,

}


export default CounterApp;